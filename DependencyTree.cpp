#include "DependencyTree.h"

void DependencyTree::analyzeFile(const fs::path &filePath, std::unordered_set<fs::path> &visitedFiles) {
    if (visitedFiles.find(filePath) != visitedFiles.end()) {
        inclusionCounts[filePath]++;
        return;
    }
    std::ifstream file(filePath);
    if (!file) {
        deletedFiles.emplace(filePath);
        return;
    }

    visitedFiles.insert(filePath);

    static std::regex includeRegex(R"(#include\s+[<"](\S+)[>"])");
    std::smatch includeMatch;

    bool isInsideMultilineComment = false;
    for (std::string line; std::getline(file, line);) {
        // Удаление комментариев в строке
        line = std::regex_replace(line, std::regex("//.*"), "");  // Удаление комментариев вида //

        if (isInsideMultilineComment) {
            // Проверка на завершение многострочного комментария
            if (line.find("*/") != std::string::npos) {
                line = line.substr(line.find("*/") + 2);
                isInsideMultilineComment = false;
            } else {
                continue;  // Пропуск строки, так как находимся внутри многострочного комментария
            }
        }

        // Удаление многострочных комментариев
        while (true) {
            size_t startPos = line.find("/*");
            size_t endPos = line.find("*/");
            if (startPos != std::string::npos && endPos != std::string::npos && startPos < endPos) {
                line = line.substr(0, startPos) + line.substr(endPos + 2);
            } else if (startPos != std::string::npos && endPos == std::string::npos) {
                line = line.substr(0, startPos);
                isInsideMultilineComment = true;
                break;
            } else {
                break;
            }
        }

        // Проверка наличия директивы #include
        if (std::regex_search(line, includeMatch, includeRegex)) {
            std::string includedFile = includeMatch[1].str();
            if(line.find('"') != std::string::npos ){
                for (const auto& entry : fs::recursive_directory_iterator(directoryPath)) {
                    if (entry.is_regular_file() && entry.path().filename() == includedFile) {
                        includedFile = entry.path().string();
                    }
                }
            } else if(!externFilePaths.empty()){
                for(const auto& path: externFilePaths) {
                    for (const auto& entry : fs::directory_iterator(path)) {
                        if (entry.is_regular_file() && entry.path().filename() == includedFile) {
                            includedFile = entry.path().string();
                            break;
                        }
                    }
                }
            }
            fs::path includedFilePath = includedFile;

            inclusionCounts[includedFilePath]++;
            inclusionMap[filePath].insert(includedFilePath);

            analyzeFile(includedFilePath, visitedFiles);
        }
    }

    file.close();
    visitedFiles.erase(filePath);
}

void DependencyTree::analyzeSourceDirectory() {
    std::unordered_set<fs::path> visitedFiles;
    for (const auto &entry: fs::recursive_directory_iterator(directoryPath)) {
        if (entry.is_regular_file() && (entry.path().extension() == ".hpp"
        || entry.path().extension() == ".h" || entry.path().extension() == ".cpp")) {
            visitedFiles.clear();
            analyzeFile(entry.path(), visitedFiles);
        }
    }
}

void DependencyTree::printInclusionFrequency() {
    std::vector<std::pair<fs::path, int>> inclusionCountsVector(
            inclusionCounts.begin(), inclusionCounts.end());
    std::sort(inclusionCountsVector.begin(), inclusionCountsVector.end(),
              [](const auto &a, const auto &b) {
                  if (a.second != b.second) {
                      return a.second > b.second;
                  } else {
                      return a.first.filename() < b.first.filename();
                  }
              }
    );
    for (const auto &pair: inclusionCountsVector) {
        std::cout << pair.first.filename() << " : " << pair.second << std::endl;
    }
}

void DependencyTree::printDependenciesWithIndentation(const fs::path& filePath, int indentationLevel,
                                                      std::unordered_set<fs::path>& visitedFiles) {
    // Проверяем, был ли файл уже посещен
    if (visitedFiles.find(filePath) != visitedFiles.end()) {
        std::cout << filePath;
        return;
    }
    visitedFiles.insert(filePath);

    if (inclusionMap.find(filePath) != inclusionMap.end()) {
        const auto& includedFiles = inclusionMap[filePath];
        for (const auto& includedFile : includedFiles) {
            std::cout << std::string(indentationLevel, '.') << includedFile.filename();
            if(deletedFiles.find(includedFile) != deletedFiles.end())
                std::cout << "(!)";
            std::cout << std::endl;
            printDependenciesWithIndentation(includedFile, indentationLevel + 2, visitedFiles);
        }
    }
    visitedFiles.erase(filePath);
}


void DependencyTree::printDependencies() {
    std::unordered_set<fs::path> visitedFiles;

    for (const auto& [filePath, _] : inclusionMap) {
        std::cout << "File: " << filePath.filename() << " included files:" << std::endl;
        printDependenciesWithIndentation(filePath, 2, visitedFiles);
    }
}

DependencyTree::DependencyTree(const fs::path &dirPath, const std::vector<fs::path>& fPaths) :
        directoryPath(dirPath), externFilePaths (fPaths)
{}



