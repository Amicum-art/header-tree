#ifndef UNTITLED_DEPENDENCYTREE_H
#define UNTITLED_DEPENDENCYTREE_H
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <filesystem>
#include <unordered_set>
#include <regex>
#include <algorithm>
#include <functional>

namespace fs = std::filesystem;

namespace std {
    template<>
    struct [[maybe_unused]] hash<fs::path> {
        std::size_t operator()(const fs::path &p) const noexcept {
            return fs::hash_value(p);
        }
    };
}

class DependencyTree {
    const fs::path directoryPath;
    std::vector<fs::path> externFilePaths;
    std::unordered_map<fs::path, std::unordered_set<fs::path>> inclusionMap;
    std::unordered_map<fs::path, int> inclusionCounts;
    std::unordered_set<fs::path> deletedFiles;
    void analyzeFile(const fs::path &filePath, std::unordered_set<fs::path> &visitedFiles);
    void printDependenciesWithIndentation(const fs::path& filePath, int indentationLevel,
                                          std::unordered_set<fs::path>& visitedFiles);
public:
    DependencyTree() = delete;
    DependencyTree(const fs::path &dirPath, const std::vector<fs::path>& fPaths);
    void analyzeSourceDirectory();

    void printInclusionFrequency();


    void printDependencies();
};


#endif //UNTITLED_DEPENDENCYTREE_H
