#include <gtest/gtest.h>
#include "DependencyTree.h"


int MyMain(int argc, char *argv[]) {
    if (argc < 2) {
        std::cout << "directory not found\n";
        return -1;
    }
    if (!std::filesystem::exists(argv[1]) || !std::filesystem::is_directory(argv[1])) {
        std::cout << argv[1] << " is not directory\n";
        return -1;
    }
    std::vector<fs::path> filenames;
    if (argc > 2) {
        for (int i = 2; i < argc; i++) {
            if (std::string(argv[i]) == "-I" && (i + 1 < argc)) {
                if (std::filesystem::exists(argv[i + 1]) &&
                    std::filesystem::is_directory(argv[i + 1])) {
                    filenames.emplace_back(argv[i + 1]);
                }
            }
        }
    } else {
        std::cout << "directories not found\n";
    }
    DependencyTree tree(argv[1], filenames);
    tree.analyzeSourceDirectory();

    tree.printDependencies();
    std::cout << std::endl;
    tree.printInclusionFrequency();
    return 0;
}


TEST(ProgramTest, RunWithArguments) {

    const char *argv[] = {
            "./untitled.exe",
            "./project",
            "-I",
            "othDir"
    };
    int argc = sizeof(argv) / sizeof(argv[0]);

    testing::internal::CaptureStdout();

    int error = MyMain(argc, const_cast<char **>(argv));
    EXPECT_EQ(error, 0);

    std::string output = testing::internal::GetCapturedStdout();

    std::string expectedOutput = "File: \"rec.h\" included files:\n"
                                 "..\"asd.h\"\n"
                                 "....\"foo.h\"(!)\n"
                                 "File: \"foo.h\" included files:\n"
                                 "..\"rec.h\"\n"
                                 "....\"asd.h\"\n"
                                 "......\"foo.h\"(!)\n"
                                 "..\"othFile.h\"\n"
                                 "....\"deleted.h\"(!)\n"
                                 "..\"asd.h\"\n"
                                 "....\"foo.h\"(!)\n"
                                 "File: \"othFile.h\" included files:\n"
                                 "..\"deleted.h\"(!)\n"
                                 "File: \"main.cpp\" included files:\n"
                                 "..\"Kar.h\"(!)\n"
                                 "..\"aa.h\"(!)\n"
                                 "..\"foo.h\"\n"
                                 "....\"rec.h\"\n"
                                 "......\"asd.h\"\n"
                                 "........\"foo.h\"(!)\n"
                                 "....\"othFile.h\"\n"
                                 "......\"deleted.h\"(!)\n"
                                 "....\"asd.h\"\n"
                                 "......\"foo.h\"(!)\n"
                                 "File: \"asd.h\" included files:\n"
                                 "..\"foo.h\"(!)\n"
                                 "\n"
                                 "\"foo.h\" : 6\n"
                                 "\"asd.h\" : 5\n"
                                 "\"deleted.h\" : 2\n"
                                 "\"othFile.h\" : 2\n"
                                 "\"rec.h\" : 2\n"
                                 "\"Kar.h\" : 1\n"
                                 "\"aa.h\" : 1\n"
                                 "\"foo.h\" : 1\n";
    EXPECT_EQ(output, expectedOutput);

}

int main(int argc, char *argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

