#include "DependencyTree.h"


auto main(int argc, char *argv[]) -> int {
    if (argc < 2) {
        std::cerr << "directory not found\n";
        return -1;
    }
    if (!std::filesystem::exists(argv[1]) || !std::filesystem::is_directory(argv[1])) {
        std::cerr << argv[1] << " is not directory\n";
        return -1;
    }
    std::vector<fs::path> filenames;
    if (argc > 2) {
        for (int i = 2; i < argc; i++) {
            if (std::string(argv[i]) == "-I" && (i + 1 < argc)) {
                if (std::filesystem::exists(argv[i + 1]) &&
                    std::filesystem::is_directory(argv[i + 1])) {
                    filenames.emplace_back(argv[i + 1]);
                }
            }
        }
    } else {
        std::cerr << "directories not found\n";
    }
    DependencyTree tree(argv[1], filenames);
    tree.analyzeSourceDirectory();

    tree.printDependencies();
    std::cout << std::endl;
    tree.printInclusionFrequency();

    std::cin.get();
}
